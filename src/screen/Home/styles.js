import React  from "react";
import {Dimensions,StyleSheet} from "react-native"

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

export default {
    conatiner :{
        
        
        backgroundColor:"#000",
        height:windowHeight,
        padding:10
    },
    movieCard:{
        width:windowWidth/3.9,
        height:windowHeight/5.4,
        backgroundColor:"#fff",
      
        },
    image:{
        height:windowHeight/5.8,
        width:windowWidth/3.8,
        alignSelf:"center",
        borderRadius:10
    },
    MovieContainer:{
        alignContent:"center",
        justifyContent:"center",
        alignItems:"center",
        marginBottom:100
    },
    textStyle:{
        fontSize:20,
        fontWeight:"bold",
        color:"#fff",
        padding:10,
        marginLeft:15
    },
    movieCard2:{
        width:windowWidth/1.1,
        height:windowHeight/5.4,
        backgroundColor:"#3d3d3d",
        padding:5,
        flexDirection:"row",
        borderRadius:10
  
        
        },
        btnContainer:{
            position:"absolute",
            marginTop:windowHeight/1.1,
            flexDirection:"row",
            alignSelf:"center"
        },
        btnStyle:{
            backgroundColor:"#fff",
            width:150,
            height:40,
            alignItems:"center",
            borderRadius:20,
            justifyContent:"center",
            borderWidth:2,
            elevation:5,
            borderColor:"#000"
        }

}