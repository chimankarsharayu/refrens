import SplashScreen from "../screen/Splash/Splash"
import Home from "../screen/Home/Home"
import DetailScreen from "../screen/DetailScreen/DetailScreen"
import SearchScreen from "../screen/SearchScreen/SearchScreen"
import NextPrevScreen from "../screen/NextPrevScreen/NextPrevScreen"
import { createStackNavigator } from "@react-navigation/stack";
import { connect } from "react-redux";
import React,{Component} from "react"


const Stack = createStackNavigator();


const StackApp = () =>{
    return(
    <Stack.Navigator
    screenOptions={{
      headerShown: false
  }}
  initialRouteName = "SplashScreen"
    >
    
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
        />
        <Stack.Screen
          name="Home"
          component={Home}
        />
    
        <Stack.Screen
          name="DetailScreen"
          component={DetailScreen}
        />
        <Stack.Screen
          name="SearchScreen"
          component={SearchScreen}
        />
        <Stack.Screen
          name="NextPrevScreen"
          component={NextPrevScreen}
        />
    </Stack.Navigator>
    )
}


class StackNavigation extends Component{
    constructor(props){
        super(props)

    }
    render(){
        return <StackApp/>
    }
}

const mapStateToProps = ({ }) => {
    return {

    };
  };
  
  export default connect(
    mapStateToProps,
    null
  )(StackNavigation);
  