import { persistCombineReducers } from "redux-persist";
import AsyncStorage from '@react-native-async-storage/async-storage';

import CharacterReducer from "./base.reducer";

const config = {
  key: "root",
  storage: AsyncStorage,
  keyPrefix: '',
  timeout: null,
  whitelist: [
    "CharacterReducer",

  ]
};
const reducers = persistCombineReducers(config, {
  CharacterReducer,
 


});


const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  // console.log("IUHINHDLLHJKD", state)


  return reducers(state, action);
};


export default rootReducer;
