import * as ActionTypes from "../action/ActionsType";

const initialState = {
  character: null,
  isCharacterFetching: false,
  characterMessage: null,
  characterActionType: null,
};

export default function base(state = initialState, action) {
  switch (action.type) {
      case ActionTypes.GET_CHARACTER_PENDING: {
          console.log("GET_CHARACTER_PENDING ++++  reducer")

          return {
              ...state,
              characterActionType: action.type,
              isCharacterFetching: true,
          };
      }

      case ActionTypes.GET_CHARACTER_SUCCESS: {
          console.log("GET_CHARACTER_SUCCESS ++++  reducer", action, state)
          return {
              ...state,
              characterActionType: action.type,
              characterMessage: action.message,
              character: action.payload['body'],
              isCharacterFetching: false,
              // isWishlistErr: false,
              characterStatusCode: action.payload.statusCode
          };
      }

      case ActionTypes.GET_CHARACTER_FAIL: {
          console.log("GET_CHARACTER_FAIL ++++  reducer", action, state)
          return {
              ...state,
              characterActionType: action.type,
              characterMessage: action.message,
              isCharacterFetching: false,
              characterStatusCode: action.message.statusCode
          };
      }

      default:
          return state;
  }

}