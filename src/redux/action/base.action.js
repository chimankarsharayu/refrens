
// import auth from "@react-native-firebase/auth";
// import OtpVerification from "../../screen/Member/SignIn/OtpVerification";
import * as ActionTypes from "./ActionsType";
import {
  GetAllCharacter
} from "../service/base.service";
import { CommonAction } from "./helper/CommonAction";



//LOGIN
export const getCharacter = (url, params) => {
  const actions = {
    pending: ActionTypes.GET_CHARACTER_PENDING,
    success: ActionTypes.GET_CHARACTER_SUCCESS,
    fail: ActionTypes.GET_CHARACTER_FAIL,
  };
  console.log("getCharacter <<<<<< Action  >>>>>>>>>", url, params);

  return CommonAction(url, params, actions, GetAllCharacter);

};


