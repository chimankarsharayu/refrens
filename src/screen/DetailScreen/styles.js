import React  from "react";
import {Dimensions,StyleSheet} from "react-native"

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

export default {
    conatiner :{
        backgroundColor:"#000",
        height:windowHeight,

    },
    image:{
        height:windowHeight/4,
        width:windowWidth/3,
        alignSelf:"center",
        marginTop:windowHeight/20
    },
    greyCard:{
        width:windowWidth/4,
        height:windowHeight/23,
        backgroundColor:"#414142",
        justifyContent:"center",
        borderRadius:50,
        flexDirection:"row"
    },
    title:{
        color:"white",
        padding:5,
        fontSize:23,
        fontWeight:"bold",
        marginLeft:windowWidth/20
    },
    htmlContainer:{
        width:windowWidth/1.1,
        justifyContent:"center",
        alignSelf:"center"
    }
   
}