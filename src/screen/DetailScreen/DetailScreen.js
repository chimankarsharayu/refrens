import React,{Component} from "react"
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions,
    TextInput,
    ImageBackground,
    StatusBar,
    ScrollView,
    Image
} from "react-native"
import styles from "./styles";
import Icon from "react-native-vector-icons/Feather"
import StarIcon from "react-native-vector-icons/AntDesign"
import { RadioButton } from 'react-native-paper';
import HTML from 'react-native-render-html';
import DotIcon from "react-native-vector-icons/Octicons"



const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

class DetailScreen extends Component {
    constructor(props){
        super(props);
        this.state={
           
        }
    }

//    source = this.props.route.params.item.show.summary
    render(){
        return(
            <>
            {/* <StatusBar translucent backgroundColor="transparent" /> */}

            <View style={styles.conatiner}>

               {/*  */}
               <View style={{position:"absolute",padding:10}} >
                   <TouchableOpacity
                    onPress={()=>this.props.navigation.goBack()}
                   >
                        <Icon
                            name="arrow-left"
                            color="#fff"
                            size={30}     
                        />
                   </TouchableOpacity>
               </View>
               <View style={{justifyContent:"center"}}>
                  
                <Image
                    style={styles.image}
                    source={{uri:this.props.route.params.item.image!=null?this.props.route.params.item.image:"https://upload.wikimedia.org/wikipedia/commons/1/14/No_Image_Available.jpg?20200913095930"}}
                />
                <Text style={{color:"#fff",fontSize:16,fontWeight:"bold",alignSelf:"center",paddingTop:10}}>{this.props.route.params.item.name}</Text>

               </View>
               <View
                style={{
                    borderBottomColor: '#232324',
                    borderBottomWidth: 1,
                    paddingTop:10
                    // marginTop:windowHeight/
                }}
                />
                <View style={{flexDirection:"row",padding:20}}>
                    <View style={styles.greyCard}>
                        {/* <StarIcon
                            name="star"
                            color="#fff269"
                            size={20}
                            style={{alignSelf:"center"}}     
                        /> */}
                        {
                                   this.props.route.params.item.status == "Alive"?(
                                        <DotIcon
                                            name="dot-fill"
                                            size={20}
                                            color="green"
                                            style={{alignSelf:"center"}}     
                                        />
                                    ):(
                                        <DotIcon
                                            name="dot-fill"
                                            size={20}
                                            color="red"
                                            style={{alignSelf:"center"}}     
                                        />
                                    )
                                }
                        <Text style={{color:"white",alignSelf:"center",padding:5}}>{this.props.route.params.item.status}</Text>
                    </View>
                    <View style={[styles.greyCard,{marginLeft:10}]}>
                       
                        <Text style={{color:"white",alignSelf:"center",padding:5}}>{this.props.route.params.item.species}</Text>
                    </View>
                  
                </View>
                <View style={{padding:10,paddingLeft:10}}>
                    <Text style={{color:"#fff"}}>name : {this.props.route.params.item.name} </Text>
                    <Text style={{color:"#fff"}}>type : {this.props.route.params.item.type} </Text>
                    <Text style={{color:"#fff"}}>gender : {this.props.route.params.item.gender} </Text>
                    <Text style={{color:"#fff"}}>Origin : {this.props.route.params.item.origin.name} </Text>
                    <Text style={{color:"#fff"}}>Location : {this.props.route.params.item.location.name} </Text>
                </View>

                {/* <ScrollView>
                    
                    <View style={styles.htmlContainer}>
                       <Text style={{color:"#fff"}}>ksdbfj dsds dskjf dksjf kjfdg lkfg kfdng</Text>
                    </View>
                    
                    
                </ScrollView> */}
            </View>
            </>
        );
    }
}

export default DetailScreen