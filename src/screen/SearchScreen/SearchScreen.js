SearchScreen
import React,{Component} from "react"
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    TextInput,
    ScrollView,
    FlatList
} from "react-native"
import styles from "./styles"

import { connect } from "react-redux";
import { ActionCreators } from "../../redux/action/index";
import { bindActionCreators } from "redux";
import SearchIcon from "react-native-vector-icons/FontAwesome"
import DotIcon from "react-native-vector-icons/Octicons"

import Header from "../Header/Header";
import axios from "axios";



const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;




class SearchScreen extends Component {

    constructor(props){
        super(props);
        this.state={
           searchKey:""
        }
    }


    

  

    async searchMovies(){
        await axios.get("https://rickandmortyapi.com/api/character/?name="+this.state.searchKey).then(res => {
            if (res.status == 200) {
                console.log("Movies Data")
                this.props.navigation.navigate("SearchScreen",{
                    item:res.data
                })

            }
        }).catch(e => console.log("ERROR",e))
    }

    renderMovies = ({item})=>{

        return(
            <View style={{padding:7,}}>

                <TouchableOpacity style={styles.movieCard2}
                    onPress={()=>{
                   
                   this.props.navigation.navigate("DetailScreen",
                    {
                      
                      item: item,
                     }
                    )
                   }}
                    >
                        <Image 
                         style={styles.image}
                         source={{uri:item.image}}
                        />
                        <View style={{marginLeft:10}}>
                            <Text style={{fontSize:18,fontWeight:"bold",color:"#ffffff"}}>{item.name}</Text>
                            <View style={{flexDirection:"row"}}>
                                
                        
                                <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>Status - {item.status}</Text>
                                {
                                    item.status == "Alive"?(
                                        <DotIcon
                                            name="dot-fill"
                                            size={20}
                                            color="green"
                                            style={{marginLeft:10,marginTop:2}}
                                        />
                                    ):(
                                        <DotIcon
                                            name="dot-fill"
                                            size={20}
                                            color="red"
                                            style={{marginLeft:10,marginTop:2}}
                                        />
                                    )
                                }
                                
                            </View>
                            <Text style={{fontSize:16,fontWeight:"bold",color:"#ffffff"}}>Species - {item.species}</Text>
                            
                        </View>
                     {/* <Text style={{fontWeight:"bold", fontSize:45,alignSelf:"center",justifyContent:"center",alignItems:"center",alignContent:"center"}}>{item.alphabet}</Text> */}
                </TouchableOpacity>
                
            </View>
            )
        
    }

    // Next page
    async onClickNext (){
        await axios.get(this.props.route.params.item.info.next).then(res => {
            if (res.status == 200) {
                console.log("Movies Data")
                this.props.navigation.navigate("NextPrevScreen",{
                    item:res.data
                })

            }
        }).catch(e => console.log("ERROR",e))
    }

    //previous page
    async onClickPrev (){
        await axios.get(this.props.route.params.item.info.prev).then(res => {
            if (res.status == 200) {
                console.log("Movies Data")
                this.props.navigation.navigate("NextPrevScreen",{
                    item:res.data
                })

            }
        }).catch(e => console.log("ERROR",e))
    }
 

    render(){
        return(
           
            <View style={styles.conatiner}>
                 <View >
               
                    <TextInput 
                        value={this.state.search}
                        style={{backgroundColor:"#7d7d7d",borderRadius:10,paddingLeft:20}} 
                        placeholder="Search"
                        onChangeText={(value)=>this.setState({
                            searchKey:value
                        })}
                    />
                    <TouchableOpacity 
                    onPress={()=>this.searchMovies()}
                    style={{position:"absolute",marginLeft:windowWidth/1.2,paddingTop:10}}>
                        <SearchIcon
                            name="search"
                            size={25}
                            color="#bf0000"
                        />
                    </TouchableOpacity>
                    <View
                    style={{
                        borderBottomColor: '#232324',
                        borderBottomWidth: 1,
                        // marginTop:windowHeight/15
                    }}
                    />
                </View>
                <Text style={styles.textStyle}>Characters</Text>
                <View style={styles.MovieContainer}>
                    
                    <FlatList              
                        data={this.props.route.params.item.results}
                        numColumns={1}
                        keyExtractor={item => item.id}
                        renderItem={this.renderMovies}  
                    />
               </View>
               {/* <View style={styles.btnContainer}>
                   {
                       this.props.route.params.item.info.prev !=null ? (
                        <TouchableOpacity 
                        style={styles.btnStyle}
                        onPress={()=>this.onClickPrev()}
                        > 
                           <Text style={{color:"#bf0000",fontWeight:"bold"}}>Previous</Text>
                       </TouchableOpacity>
                       ):
                      null
                   }
                   

                   {
                        this.props.route.params.item.info.next !=null ?(

                            <TouchableOpacity 
                            style={styles.btnStyle}
                            onPress={()=>this.onClickNext()}
                            > 
                                <Text style={{color:"#bf0000",fontWeight:"bold"}}>Next</Text>
                            </TouchableOpacity>
                        ):null
                   }

                  
               </View> */}
            </View>
            
        );
    }
}




function mapStateToProps({ CharacterReducer }) {
    console.log("++++++++++++ movieReducer++++",CharacterReducer.character.info.prev)
    return {
     character:CharacterReducer.character.results,
     nextPage:CharacterReducer.character.info.next,
     prevPage:CharacterReducer.character.info.prev
    };
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(SearchScreen);